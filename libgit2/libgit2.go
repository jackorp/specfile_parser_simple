package libgit2

// #cgo pkg-config: libgit2
// #include <git2/global.h>
// #include <git2/index.h>
// #include <git2/repository.h>
// #include <git2/errors.h>
import "C"
import (
	"log"
	"unsafe"
)

func checkErrCode(err_code int, action string) {
	err := C.git_error_last()

	if err_code == 0 {
		return;
	}

	c_message := err.message

	message := C.GoString(c_message)

	log.Fatal("Error ", err_code, " ", action, ": ", message)
}

func InitGit() int {
	ret := C.git_libgit2_init()

	return int(ret)
}

func ShutdownGit() int {
	ret := C.git_libgit2_shutdown()

	return int(ret)
}

func gitOpenRepo(path string) (*C.struct_git_repository) {
	var repo *C.struct_git_repository

	repo_path := C.CString(path)
	defer C.free(unsafe.Pointer(repo_path))

	err := C.git_repository_open(&repo, repo_path)


	checkErrCode(int(err), "open repo")

	return repo
}

func GitLSFiles(repo_path string) (paths []string) {
	var index *C.struct_git_index

	repo := gitOpenRepo(repo_path)

	err := C.git_repository_index(&index, repo)
	defer C.free(unsafe.Pointer(index))

	checkErrCode(int(err), "repository index")

	entry_count_c_size_t := C.git_index_entrycount(index)
	entry_count := uintptr(entry_count_c_size_t)

	var i uintptr
	for i = 0; i < entry_count; i++ {
		entry := C.git_index_get_byindex(index, C.size_t(i))
		if entry == nil {
			log.Fatal("Internal error: Tried to get file in repo that's out of index bounds.")
		}

		paths = append(paths, C.GoString(entry.path))
	}

  return paths
}
