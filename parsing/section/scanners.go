package section

import (
	"bufio"
	"strings"
)

// Scan a section line by line. execute the hooks passed in.
// You probably want other Scan* functions.
func ScanSection(specfile, section_start, section_end string, hooks SectionHook) {
	section_index_start := strings.Index(specfile, section_start)
	section_index_end := strings.Index(specfile, section_end)
	section := specfile[section_index_start:section_index_end]
	section_scanner := bufio.NewScanner(strings.NewReader(section))
	section_scanner.Split(bufio.ScanLines)

	for section_scanner.Scan() {
		text := section_scanner.Text()
		for _, hook := range(hooks.HookCollection) {
			hook(text)
		}
	}
}
func ScanPreamble(specfile string) {
	ScanSection(specfile, "", "%prep", preambleHooks)
}

func ScanPrep(specfile string) {
	ScanSection(specfile, "%prep", "%build", prepHooks)
}

func ScanBuild(specfile string) {
	ScanSection(specfile, "%build", "%install", buildHooks)
}

func ScanInstall(specfile string) {
	ScanSection(specfile, "%install", "%check", installHooks)
}

func ScanCheck(specfile string) {
	ScanSection(specfile, "%check", "%changelog", checkHooks)
}

func ScanAllSections(specfile string) {
	section_funcs := []func(string){ScanPreamble, ScanPrep, ScanBuild, ScanInstall, ScanCheck}

	for _, section := range(section_funcs) {
		section(specfile)
	}
}
