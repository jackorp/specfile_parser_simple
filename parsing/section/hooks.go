package section

type SectionHook struct {
	// Hooks that get called with each line during scanning each phase. First argument
	// passed into function is the line.
	HookCollection []func(string)
}

var preambleHooks, prepHooks, buildHooks, installHooks, checkHooks SectionHook

func RegisterHook(section_hook *SectionHook, hook func(string)) {
	if section_hook == nil {
		section_hook = &SectionHook{HookCollection: make([]func(string), 0)}
	}
	section_hook.HookCollection = append(section_hook.HookCollection , hook)
}

func RegisterAllSectionHook(hook func(string)) {
	hooks := []*SectionHook{&preambleHooks, &prepHooks, &buildHooks, &installHooks, &checkHooks}

	for _, hook_collection := range(hooks) {
		RegisterHook(hook_collection, hook)
	}
}

func RegisterPreambleHook(hook func(string)) {
	RegisterHook(&preambleHooks, hook)
}

func RegisterPrepHook(hook func(string)) {
	RegisterHook(&prepHooks, hook)
}

func RegisterBuildHook(hook func(string)) {
	RegisterHook(&buildHooks, hook)
}

func RegisterInstallHook(hook func(string)) {
	RegisterHook(&installHooks, hook)
}

func RegisterCheckHook(hook func(string)) {
	RegisterHook(&checkHooks, hook)
}
