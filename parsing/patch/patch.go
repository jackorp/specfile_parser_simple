package patch

import (
	"log"
	"strconv"
	"strings"
)

type Patch struct {
	Number    int
	Name      string
	Applied   bool
	Is_in_git bool
}

func ParsePatchDefinition(line string) Patch {
	num_name := line[len("Patch"):]
	split_num_name := strings.SplitN(num_name, ":", 2)

	num, err := strconv.ParseInt(split_num_name[0], 10, 0)
	if err != nil {
		log.Fatal("Could not convert patch number. String: \"", num, "\"\n", err)
	}
	name := strings.TrimSpace(split_num_name[1])

	return Patch{Number: int(num), Name: name, Applied: false, Is_in_git: false}
}
