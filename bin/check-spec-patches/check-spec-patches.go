package main

import (
	"bufio"
	"flag"
	"fmt"
	"gitlab.com/jackorp/specfile_parser_simple/libgit2"
	"gitlab.com/jackorp/specfile_parser_simple/parsing/patch"
	hooks "gitlab.com/jackorp/specfile_parser_simple/parsing/section"
	scanners "gitlab.com/jackorp/specfile_parser_simple/parsing/section"
	"log"
	"os"
	"strconv"
	"strings"
)

func AppliedPatchDefinition(line string, patches []patch.Patch) {
	rest := line[len("%patch"):]

	patchline_scanner := bufio.NewScanner(strings.NewReader(rest))
	patchline_scanner.Split(bufio.ScanWords)

	var patch_num int
	patch_num = -1

	parser_expect_int := false

	for patchline_scanner.Scan() {
		part := patchline_scanner.Text()
		// Here we have to account for the 2 major forms, which is %patchN and
		// %patch -P N (and the %patch -PN variation). There is also "%patch N" form, but I don't respect it.
		// Though my current approach might parse it accidentally, since I skip the %patch and bufio.ScanWords
		// ignores leading spaces.
		possible_patch_num, err := strconv.ParseInt(part, 10, 0)
		if err == nil {
			patch_num = int(possible_patch_num)
			break
		} else if parser_expect_int {
			// err was not nil and parser was expecting int...
			// Don't mind the % related warning if you encounter it, golang is annoying sometimes.
			fmt.Println("Possibly incorrect %patch definition. Expected integer.",
			            "Tried to parse '" + part + "' as numerical argument for -P.")
			parser_expect_int = false
			break
		}

		if strings.HasPrefix(part, "-P") {
			// It probably is -PN form, so discard -P and parse the number.
			if len(part) > 2 {
				possible_patch_num, err := strconv.ParseInt(part[len("-P"):], 10, 0)
				if err == nil {
					patch_num = int(possible_patch_num)
					break
				}
			} else {
				// It can also be -P N form, let's try on another chunk.
				parser_expect_int = true
			}
		}
	}

	for i, patch := range patches {
		if patch_num == patch.Number {
			patches[i].Applied = true
		}
	}
}

func patchfilesInGit(path string) (patches_in_git []string) {
	libgit2.InitGit()
	defer libgit2.ShutdownGit()

	files_in_git := libgit2.GitLSFiles(path)

	for _, file := range(files_in_git) {
		if strings.HasSuffix(file, ".patch") {
			patches_in_git = append(patches_in_git, file)
		}
	}

	return patches_in_git
}

func patchesInGit(declared_patches []patch.Patch, git_dir string) (unapplied_patches []patch.Patch) {
	unapplied_patchfiles := patchfilesInGit(git_dir)

	for i, patch := range(declared_patches) {
		found := false
		git_patch_idx := -1
		for ii, git_patchfile := range(unapplied_patchfiles) {
			if patch.Name == git_patchfile {
				declared_patches[i].Is_in_git = true
				found = true
				git_patch_idx = ii
				break
			}
		}

		if found {
			unapplied_patchfiles = append(unapplied_patchfiles[:git_patch_idx], unapplied_patchfiles[git_patch_idx+1:]...)
			found = false
		}
	}

	for _, patchfile := range(unapplied_patchfiles) {
		patch := patch.Patch{Number: -1, Name: patchfile, Applied: false, Is_in_git: true}
		unapplied_patches = append(unapplied_patches, patch)
	}

	return unapplied_patches
}

func printUnusedPatches(patches []patch.Patch, name_only bool) {
	fmt.Println("====", "Unused Patches present in git", "====")
	formatted_unused_patches := ""
	for _, patch := range(patches) {
		formatted_unused_patches = formatted_unused_patches + patch.Name + "\n"
	}
	fmt.Print(formatted_unused_patches)
	fmt.Println("---- ----")
}

func printUncommittedPatches(patches []patch.Patch, name_only bool) {
	fmt.Println("====", "Uncommitted applied patches", "====")
	formatted_uncommitted_patches := ""

	for _, patch := range(patches) {
		var prefix string
		if !name_only {
			prefix = "Patch" + strconv.FormatInt(int64(patch.Number), 10) + " "
		}

		formatted_uncommitted_patches = formatted_uncommitted_patches + prefix + patch.Name + "\n"
	}
	fmt.Print(formatted_uncommitted_patches)
	fmt.Println("---- ----")
}

func printUnappliedPatches(patches []patch.Patch, name_only bool) {
	fmt.Println("====", "Unapplied but declared patches", "====")
	formatted_unapplied_patches := ""

	for _, patch := range(patches) {
		var prefix string
		if !name_only {
			prefix = "Patch" + strconv.FormatInt(int64(patch.Number), 10) + " "
		}

		formatted_unapplied_patches = formatted_unapplied_patches + prefix + patch.Name + "\n"
	}
	fmt.Print(formatted_unapplied_patches)
	fmt.Println("---- ----")
}

func main() {
	var print_name_only bool
	flag.BoolVar(&print_name_only, "name-only", print_name_only, "Print patches only with name per line, without PatchN prefix.")
	flag.Parse()

	spec_path := flag.Arg(0)

	spec_file, err := os.ReadFile(spec_path)
	if err != nil {
		log.Fatal("Could not open specfile '", spec_path, "'")
	}
	specfile := string(spec_file)

	var declared_patchfiles []patch.Patch

	declare_patchfile := func(text string) {
		if strings.HasPrefix(text, "Patch") {
			declared_patchfiles = append(declared_patchfiles, patch.ParsePatchDefinition(text))
		}
	}
	apply_patch_definitions := func(text string) {
		if strings.HasPrefix(text, "%patch") {
			AppliedPatchDefinition(text, declared_patchfiles)
		}
	}

	hooks.RegisterPreambleHook(declare_patchfile)
	hooks.RegisterAllSectionHook(apply_patch_definitions)

	scanners.ScanAllSections(specfile)

	var patch_declared_but_unapplied []patch.Patch

	for _, patch := range(declared_patchfiles) {
		if !patch.Applied {
			patch_declared_but_unapplied = append(patch_declared_but_unapplied, patch)
		}
	}

	var there_was_error bool

	if len(patch_declared_but_unapplied) == 0 {
		fmt.Println("All declared patches are applied ✓")
	} else {
		printUnappliedPatches(patch_declared_but_unapplied, print_name_only)
		there_was_error = true
	}

	var patch_applied_not_in_git []patch.Patch

	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	unused_patches := patchesInGit(declared_patchfiles, cwd)

	for _, patch := range(declared_patchfiles) {
		if patch.Applied && !patch.Is_in_git {
			patch_applied_not_in_git = append(patch_applied_not_in_git, patch)
		}
	}

	if len(patch_applied_not_in_git) == 0 {
		fmt.Println("All applied patches are in git ✓")
	} else {
		printUncommittedPatches(patch_applied_not_in_git, print_name_only)
		there_was_error = true
	}

	if len(unused_patches) == 0 {
		fmt.Println("No extra patches in git ✓")
	} else {
		printUnusedPatches(unused_patches, print_name_only)
		there_was_error = true
	}

	if there_was_error {
		os.Exit(1)
	}
}
