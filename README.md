# Specfile Parser Simple

`Specfile parser simple` is a long name for simple project. The target is to go through each
section line-by-line and statically analyze the specfile for some simple patterns.

It is meant to be used as both a library and an executable. Executable currently parses specfile to
check for declaration, application and presence of patches. For this end, there is also a limited
integration with libgit2 library.

## Installing

This library and the separate `check-spec-patches` utility is currently available as an RPM
installable from copr, to enable it on Fedora/RPM based systems, see: https://copr.fedorainfracloud.org/coprs/jackorp/jackorps-golang-utils/

To enable and install the check-spec-patches, you run:
```console
$ sudo dnf copr enable jackorp/jackorps-golang-utils -y
$ sudo dnf install check-spec-patches -y
```
These commands enable the copr repository where this project is distributed and then installs the
built `check-spec-patches` binary, together with required dependencies.

## Using the CLI to check patches

### Building the binary

To build `check-spec-patches`, you need to have libgit2-devel installed, do so by running:
```console
$ sudo dnf install libgit2-devel
```
This brings in the necessary files to compile in the git integration.

Next, in project's root, run:
```
$ go build bin/check-spec-patches
```
This will create the `check-spec-patches` file in the directory.

### Running the binary

By default you have to provide the specfile. Provide it as the last argument.
You also have the following options that you have to specify right after the binary:
* `--name-only` or `-name-only` Print patches only with name per line, without PatchN prefix.
  For patch named `foo-1.2.3.patch`, the line will only contain `foo-1.2.3.patch`.
  If this option is not specified then it will be: `Patch0 foo-1.2.3.patch` if the patch is declared
  as Patch0 in the specfile.

Make sure you are running the binary in the directory of the specfile. The git integration currently
takes into account the the current working directory from where the binary was called.

If everything is OK, the following can be observed:
```console
$ ./check-spec-patches foo.spec
All declared patches are applied ✓
All applied patches are in git ✓
No extra patches in git ✓
```

If there is some problem, the following can be observed (in this case, extra patch `foo.patch` in git):
```console
$ ./check-spec-patches foo.spec
All declared patches are applied ✓
All applied patches are in git ✓
==== Unused Patches present in git ====
b.patch
---- ----
```
In the case of patches that are unapplied, missing in git if used or extra in git if unused, or in
the case of internal errors of the tool, the exit code will be 1.

!!! NOTE: The program is only looking at patches that are suffixed with `.patch`. So if your
patchfile has a different suffix, it might not get correctly detected.

## Using the library

_TODO: make actual godocs. (and fix the potential out-of-bounds accesses)_

Library provides several facilities for parsing specfiles. I recommend looking into the main
function of the `bin/check-spec-patches/check-spec-patches.go` file. You can define hooks, which have the function
signature of `func (string) ()`, IOW, accepts string which is the current line the scanner has and
returns nothing. These hooks can be define per-section of the specfile or for all section via the
`RegisterAllSectionHook` function. Similarly, you can scan section-by-section by calling the
individual corresponding functions or scan all sections at once with the `ScanAllSections` function.
Additionally, section scanner file also provides `ScanSection` function that allows you to scan a
custom range in the specfile. In that event you are expected to define your own hook collection you
want to execute.
